"""Crawl mailarchives and download patches."""
from datetime import datetime
import glob
import gzip
import http.cookiejar
import io
import os
import re
import urllib.error
import urllib.parse
import urllib.request

from bs4 import BeautifulSoup as BS
import requests

from cki_lib.logger import file_logger


class MailArchiveCrawler:
    # pylint: disable=too-many-instance-attributes
    """Crawl mailarchives."""

    def __init__(self, baseurl, username, cookie_file, try_login=False):
        """Instantiate a mail archive crawler.

        If cookie_file doesn't exist, it will be created.

        Args:
            baseurl: server url, including mailing list
            username: login username (usually includes @redhat.com)
            cookie_file: path to a cookie file
        Attributes:
            cj: cookielib LWPCookieJar instance, helper object for
                saving/loading/using cookies
            opener: helper object to open urls using cookies

        """
        self.logger = file_logger(__name__)
        self.skipped_logger = file_logger('skipped', dst_file='skipped.log')

        self.url_msg_regex = re.compile(r'^http.*?/msg[0-9]+\.html$')
        self.ym_summary_regex = re.compile(r'^([0-9]+-.*?)\..*?$')

        self.overall_result_rgx = re.compile(
            r'^\s+Overall result: (FAILED|PASSED|SKIPPED).*?$', re.IGNORECASE)

        self.baseurl = baseurl
        self.username = username

        self.cookie = http.cookiejar.LWPCookieJar(cookie_file)

        needs_login = False

        try:
            # try to load cookie file
            self.cookie.load(ignore_discard=True, ignore_expires=True)
        except (ValueError, IOError):
            # no cookie file - we need to login first
            needs_login = True

        # create opener using cj helper object
        self.opener = urllib.request.build_opener(
            urllib.request.HTTPCookieProcessor(self.cookie))

        if needs_login:
            if try_login:
                # ask for password and login into mail archive
                self.do_login()

            # save cookie file, it's important to ignore expires and discards
            self.cookie.save(cookie_file, ignore_expires=True,
                             ignore_discard=True)

    def do_login(self):
        """Login to mailarchives webui."""
        password = input('enter password: ')

        # craft login message
        login_data = urllib.parse.urlencode(
            {
                'username': self.username,
                'password': password,
                'submit': 'Check'
            }
        )

        self.opener.open(self.baseurl, bytearray(login_data, 'utf-8'))

    def get_archives_months(self):
        """Get a list of months we have gziped files for online."""
        result = self.opener.open(self.baseurl)

        # get initial page after login
        html = result.read()

        soup = BS(html, "lxml")

        results = []
        for elem in soup.findAll('a'):
            match = self.ym_summary_regex.match(elem['href'])
            if match and "Gzip'd Text" in elem.text:
                results.append((match.group(1), elem['href']))

        return results

    def get_cached_files(self, path, suffix='*.txt'):
        """Get files in dir with suffix that match date format of archives.

        Arguments:
            path: a path to the directory where to look for files (str)
            suffix: a suffix of files to test (str), default: *.txt
        Returns:
            a list of files
        """
        files = glob.glob(os.path.join(path, suffix))
        files = [os.path.basename(f) for f in files]
        return [f for f in files if self.ym_summary_regex.match(f)]

    def download_files(self, online_refs, dest_path, overwrite=True):
        """Download a list of mailarchive urls to dest_path."""
        if not os.path.isdir(dest_path):
            os.makedirs(dest_path, exist_ok=True)

        for name, href in online_refs:
            # craft path to gzip
            weburl = os.path.join(self.baseurl, href)
            # path to local file
            dst_file = os.path.join(dest_path, href.replace('.gz', ''))

            if not overwrite and os.path.isfile(dst_file):
                msg = f'{name} exists, skipping'
            else:
                msg = f'{href} will be downloaded'

                # no cached file, download is necessary
                response = requests.get(weburl)

                with gzip.open(io.BytesIO(response.content),
                               'rb') as filehandle:
                    with open(dst_file, 'wb') as dsthandle:
                        dsthandle.write(filehandle.read())

            # let user know what's up
            self.logger.info(msg)

    def update_listarchive(self, dest_path, delete_latest=False,
                           updatelastmonth=False):
        """Update cached list archive files in dest_path."""
        cached_files = self.get_cached_files(dest_path)

        online_refs = self.get_archives_months()
        if updatelastmonth:
            online_refs = [online_refs[0]]

        if delete_latest:
            try:
                latest = list(sorted([datetime.strptime(f, '%Y-%B.txt') for
                                      f in cached_files]))[-1]
            except IndexError:
                pass
            else:
                latest = latest.strftime('%Y-%B.txt')

                self.logger.info('Removing latest file "%s"', latest)

                os.remove(os.path.join(dest_path, latest))

        self.download_files(online_refs, dest_path)
