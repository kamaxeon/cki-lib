"""Implement secrets and variable access as used in the infrastructure repo."""
import functools
import os
import pathlib
import subprocess
import typing

import yaml


@functools.lru_cache
def _read_secrets_file(file_path: str) -> typing.Dict[str, typing.Any]:
    return yaml.safe_load(pathlib.Path(file_path).read_text())


def _read_secrets_files(env_names: typing.Sequence[str]) -> typing.Dict[str, typing.Any]:
    result = {}
    for env_name in env_names:
        if env_name not in os.environ:
            continue
        result.update(_read_secrets_file(os.environ[env_name]))
    return result


def encrypt(value: str, *, salt: str = None) -> str:
    """Encrypt a secret."""
    fixed_salt = [] if salt is None else ['-S', salt]
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt',
        '-pass', 'env:ENVPASSWORD',
        '-e', '-a'
    ] + fixed_salt, encoding='utf8', input=value, capture_output=True, check=True)
    return process.stdout.strip()


def decrypt(value: str) -> str:
    """Decrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt',
        '-pass', 'env:ENVPASSWORD',
        '-d', '-a'
    ], encoding='utf8', input=value + '\n', capture_output=True, check=True)
    return process.stdout.strip()


def _get(key: str, env_names: typing.Sequence[str], encrypted: bool) -> typing.Any:
    value = _read_secrets_files(env_names)[key]
    if not isinstance(value, str):
        return value
    if not value.startswith('U2FsdGVkX1'):
        return value.strip()
    if not encrypted:
        raise Exception(f'Unable to access secret {key} via get_variable')
    return decrypt(value)


def get_secret(key: str) -> typing.Any:
    """Return a decrypted secret from the secrets file.

    Also supports unencrypted values (deprecated).
    """
    return _get(key, ('SECRETS_FILE',), True)


def get_variable(key: str) -> typing.Any:
    """Return an unencrypted variable from the variables files.

    Also supports unencrypted variables in the secrets file (deprecated).
    """
    return _get(key, ('GLOBAL_VARS_FILE', 'INTERNAL_VARS_FILE', 'SECRETS_FILE'), False)
