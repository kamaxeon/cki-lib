"""Expose multi-process metrics collected from env[prometheus_multiproc_dir]."""
import prometheus_client
import prometheus_client.multiprocess


def run(_, start_response):
    """Run main handler."""
    registry = prometheus_client.CollectorRegistry()
    prometheus_client.multiprocess.MultiProcessCollector(registry)
    data = prometheus_client.generate_latest(registry)
    status = '200 OK'
    response_headers = [
        ('Content-type', prometheus_client.CONTENT_TYPE_LATEST),
        ('Content-Length', str(len(data)))
    ]
    start_response(status, response_headers)
    return iter([data])
