"""Teiid connector tester."""
import logging
import unittest
from unittest import mock

from cki_lib import teiid
from cki_lib.logger import file_logger

LOGGER = file_logger(__name__, dst_file='ps_teiid_info.log',
                     stream_level=logging.INFO)


# pylint: disable=W0212,R0201


class TestTeiid(unittest.TestCase):
    """Test Teiid connector class."""

    def setUp(self):
        """Bring up tests."""
        self.html_testdata = """<tr/><tr><th>Keyvalue</th><th>Operation</th>
        <th>Value</th></tr>"""

    @mock.patch('cki_lib.teiid.LOGGER.info', lambda *x: None)
    def test_in_out(self):
        """Test data processing."""
        # Write html data to stdout, read it, process it, quit on timeout (1s)
        conn = teiid.TeiidConnector(f'echo "{self.html_testdata}"',
                                    timeout=1)
        proc = conn.run()
        proc.join()
        proc.terminate()

        # test with real data -> real output
        result = conn.results.get()
        self.assertEqual(([[]], ['Keyvalue', 'Operation', 'Value']),
                         result)

    @mock.patch('cki_lib.teiid.LOGGER.info', lambda *x: None)
    def test_teiid_stderr(self):
        """Ensure teiid breaks out on known errors."""

        script = """import sys,time
wrote = False
while True:
    if not wrote:
        sys.stderr.write('duplicate key value violates unique constraint\n')
        wrote = True
        time.sleep(1)
        """
        conn = teiid.TeiidConnector(f'''python3 -c "{script}"''')
        proc = conn.run()
        proc.join()
        proc.terminate()

    def test_html2rows_cols(self):
        """Ensure html2rows_cols works."""
        result = teiid.TeiidConnector.html2rows_cols(self.html_testdata)

        self.assertEqual(result, ([[]], ['Keyvalue', 'Operation', 'Value']))

    @mock.patch('cki_lib.teiid.LOGGER.info', lambda *x: None)
    @mock.patch('cki_lib.teiid.LOGGER.warning', lambda *x: None)
    def test_handle_stderr(self):
        """Ensure _handle_stderr works."""
        tuples = [
            # this error string must break execution
            ('duplicate key value violates unique constraint', False, True, False),
            # this error string must NOT break execution
            ('could not find a "psql" to execute', True, False, False),
            # don't break execution
            ('', True, False, False),
            # break execution on unknown error
            ('blah', False, False, False),
            # don't break execution on invalid query
            ('Was expecting: ', True, True, False)
        ]

        for tup in tuples:
            msg, expected_flags = tup[0], tup[1:]
            self.assertEqual(expected_flags, teiid._handle_stderr(msg))

        # warning was printed
        self.assertTrue(teiid._handle_stderr.psql_warn_printed)

    def test_is_output_done(self):
        """Ensure is_output_done works."""
        self.assertTrue(teiid.is_output_done('INSERT'))
        self.assertTrue(teiid.is_output_done('row)'))
        self.assertTrue(teiid.is_output_done('rows)'))
        self.assertTrue(teiid.is_output_done('ERROR: '))

        self.assertFalse(teiid.is_output_done('data data'))

    @mock.patch('cki_lib.teiid.LOGGER.info', lambda *x: None)
    def test_stdout_termination(self):
        """Ensure run() terminates on specific stdout data."""
        conn = teiid.TeiidConnector('echo "INSERT"')
        proc = conn.run()

        conn.query('blah')

        proc.join()
        proc.terminate()
